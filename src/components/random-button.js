import React, { useState } from "react";
import PropTypes from "prop-types";

const RandomButton = ({ min, max, className }) => {
  const [num, setNum] = useState();
  const handleClick = () => {
    const rn = min + parseInt(Math.random() * max);
    console.log(rn);
    setNum(rn);
  };
  return (
    <button className={className} onClick={handleClick}>
      {num ? `${num} | Generate again` : "Generate Random"}
    </button>
  );
};

RandomButton.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  className: PropTypes.string,
};

RandomButton.defaultProps = {
  min: 0,
  max: 100,
};

export default RandomButton;
