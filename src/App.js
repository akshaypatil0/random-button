import React from "react";
import RandomButton from "./components/random-button";

function App() {
  return (
    <div className="App">
      <RandomButton />
    </div>
  );
}

export default App;
